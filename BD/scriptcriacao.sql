create database mygrades;

create table cursos(
idCurso int not null primary key auto_increment,
nomecurso varchar(500) not null,
anocurso int null,
iniciocurso date null,
fimcurso date null,
horas int null
);

create table usuarios(
idUsuario int not null primary key auto_increment,
nome varchar(200) not null,
usuario varchar(200) not null,
senha varchar (30) not null,
status int not null,
perfil int null
);

create table matricula(
idMatricula int not null primary key auto_increment,
idAluno int not null references usuarios,
idCurso int not null references cursos,
dtInicio date not null,
dtFim date not null
);

create table categoriasLinks(
categoria varchar(5) not null primary key,
descricaocat varchar(30) not null
);

create table links(
idLink int not null primary key auto_increment,
link varchar(500) not null,
categoria varchar(5) not null references categoriasLinks,
inclusao date null,
visivel boolean null,
curso int not null references cursos
);

create table lembretes(
idLembrete int not null primary key auto_increment,
descricaoLembrete varchar(100) not null,
dia date not null,
hora varchar(5) null,
curso int not null references cursos
);

create table atividades(
idAtividade int not null primary key auto_increment,
atividadedesc varchar(200) not null,
tipoatividade int not null,
curso int not null references cursos
);

create table notas(
idNota int not null primary key auto_increment,
idAluno int not null references usuarios,
idAtividade int not null references atividades,
completada boolean not null,
nota decimal(10,2) null
);

create table frequencia(
idfreq int not null primary key auto_increment,
idAluno int not null references usuarios,
dataChamada date not null,
presente boolean not null,
curso int not null references cursos
)
